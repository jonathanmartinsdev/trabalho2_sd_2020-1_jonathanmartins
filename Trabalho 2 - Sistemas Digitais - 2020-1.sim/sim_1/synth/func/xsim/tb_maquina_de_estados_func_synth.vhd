-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
-- Date        : Wed Oct 21 07:22:08 2020
-- Host        : DESKTOP-8A438K8 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file {E:/OneDrive/Documentos/School/Sistemas
--               Digitiais/Trabalhos/Trabalho 2/VHDL/Trabalho 2 - Sistemas Digitais - 2020-1/Trabalho 2 - Sistemas
--               Digitais - 2020-1.sim/sim_1/synth/func/xsim/tb_maquina_de_estados_func_synth.vhd}
-- Design      : maquina_de_estados
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a12tcpg238-3
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity maquina_de_estados is
  port (
    botao : in STD_LOGIC;
    reset : in STD_LOGIC;
    clk : in STD_LOGIC;
    led : out STD_LOGIC;
    display_7seg : out STD_LOGIC_VECTOR ( 6 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of maquina_de_estados : entity is true;
end maquina_de_estados;

architecture STRUCTURE of maquina_de_estados is
  signal botao_IBUF : STD_LOGIC;
  signal botao_IBUF_BUFG : STD_LOGIC;
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal cont : STD_LOGIC_VECTOR ( 25 downto 0 );
  signal \cont[25]_i_10_n_0\ : STD_LOGIC;
  signal \cont[25]_i_2_n_0\ : STD_LOGIC;
  signal \cont[25]_i_3_n_0\ : STD_LOGIC;
  signal \cont[25]_i_5_n_0\ : STD_LOGIC;
  signal \cont[25]_i_6_n_0\ : STD_LOGIC;
  signal \cont[25]_i_7_n_0\ : STD_LOGIC;
  signal \cont[25]_i_8_n_0\ : STD_LOGIC;
  signal \cont[25]_i_9_n_0\ : STD_LOGIC;
  signal \cont_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \cont_reg[12]_i_2_n_1\ : STD_LOGIC;
  signal \cont_reg[12]_i_2_n_2\ : STD_LOGIC;
  signal \cont_reg[12]_i_2_n_3\ : STD_LOGIC;
  signal \cont_reg[12]_i_2_n_4\ : STD_LOGIC;
  signal \cont_reg[12]_i_2_n_5\ : STD_LOGIC;
  signal \cont_reg[12]_i_2_n_6\ : STD_LOGIC;
  signal \cont_reg[12]_i_2_n_7\ : STD_LOGIC;
  signal \cont_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \cont_reg[16]_i_2_n_1\ : STD_LOGIC;
  signal \cont_reg[16]_i_2_n_2\ : STD_LOGIC;
  signal \cont_reg[16]_i_2_n_3\ : STD_LOGIC;
  signal \cont_reg[16]_i_2_n_4\ : STD_LOGIC;
  signal \cont_reg[16]_i_2_n_5\ : STD_LOGIC;
  signal \cont_reg[16]_i_2_n_6\ : STD_LOGIC;
  signal \cont_reg[16]_i_2_n_7\ : STD_LOGIC;
  signal \cont_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \cont_reg[20]_i_2_n_1\ : STD_LOGIC;
  signal \cont_reg[20]_i_2_n_2\ : STD_LOGIC;
  signal \cont_reg[20]_i_2_n_3\ : STD_LOGIC;
  signal \cont_reg[20]_i_2_n_4\ : STD_LOGIC;
  signal \cont_reg[20]_i_2_n_5\ : STD_LOGIC;
  signal \cont_reg[20]_i_2_n_6\ : STD_LOGIC;
  signal \cont_reg[20]_i_2_n_7\ : STD_LOGIC;
  signal \cont_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \cont_reg[24]_i_2_n_1\ : STD_LOGIC;
  signal \cont_reg[24]_i_2_n_2\ : STD_LOGIC;
  signal \cont_reg[24]_i_2_n_3\ : STD_LOGIC;
  signal \cont_reg[24]_i_2_n_4\ : STD_LOGIC;
  signal \cont_reg[24]_i_2_n_5\ : STD_LOGIC;
  signal \cont_reg[24]_i_2_n_6\ : STD_LOGIC;
  signal \cont_reg[24]_i_2_n_7\ : STD_LOGIC;
  signal \cont_reg[25]_i_4_n_7\ : STD_LOGIC;
  signal \cont_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \cont_reg[4]_i_2_n_1\ : STD_LOGIC;
  signal \cont_reg[4]_i_2_n_2\ : STD_LOGIC;
  signal \cont_reg[4]_i_2_n_3\ : STD_LOGIC;
  signal \cont_reg[4]_i_2_n_4\ : STD_LOGIC;
  signal \cont_reg[4]_i_2_n_5\ : STD_LOGIC;
  signal \cont_reg[4]_i_2_n_6\ : STD_LOGIC;
  signal \cont_reg[4]_i_2_n_7\ : STD_LOGIC;
  signal \cont_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \cont_reg[8]_i_2_n_1\ : STD_LOGIC;
  signal \cont_reg[8]_i_2_n_2\ : STD_LOGIC;
  signal \cont_reg[8]_i_2_n_3\ : STD_LOGIC;
  signal \cont_reg[8]_i_2_n_4\ : STD_LOGIC;
  signal \cont_reg[8]_i_2_n_5\ : STD_LOGIC;
  signal \cont_reg[8]_i_2_n_6\ : STD_LOGIC;
  signal \cont_reg[8]_i_2_n_7\ : STD_LOGIC;
  signal \cont_reg_n_0_[0]\ : STD_LOGIC;
  signal \cont_reg_n_0_[10]\ : STD_LOGIC;
  signal \cont_reg_n_0_[11]\ : STD_LOGIC;
  signal \cont_reg_n_0_[12]\ : STD_LOGIC;
  signal \cont_reg_n_0_[13]\ : STD_LOGIC;
  signal \cont_reg_n_0_[14]\ : STD_LOGIC;
  signal \cont_reg_n_0_[15]\ : STD_LOGIC;
  signal \cont_reg_n_0_[16]\ : STD_LOGIC;
  signal \cont_reg_n_0_[17]\ : STD_LOGIC;
  signal \cont_reg_n_0_[18]\ : STD_LOGIC;
  signal \cont_reg_n_0_[19]\ : STD_LOGIC;
  signal \cont_reg_n_0_[1]\ : STD_LOGIC;
  signal \cont_reg_n_0_[20]\ : STD_LOGIC;
  signal \cont_reg_n_0_[21]\ : STD_LOGIC;
  signal \cont_reg_n_0_[22]\ : STD_LOGIC;
  signal \cont_reg_n_0_[23]\ : STD_LOGIC;
  signal \cont_reg_n_0_[24]\ : STD_LOGIC;
  signal \cont_reg_n_0_[25]\ : STD_LOGIC;
  signal \cont_reg_n_0_[2]\ : STD_LOGIC;
  signal \cont_reg_n_0_[3]\ : STD_LOGIC;
  signal \cont_reg_n_0_[4]\ : STD_LOGIC;
  signal \cont_reg_n_0_[5]\ : STD_LOGIC;
  signal \cont_reg_n_0_[6]\ : STD_LOGIC;
  signal \cont_reg_n_0_[7]\ : STD_LOGIC;
  signal \cont_reg_n_0_[8]\ : STD_LOGIC;
  signal \cont_reg_n_0_[9]\ : STD_LOGIC;
  signal estado : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal led_OBUF : STD_LOGIC;
  signal led_reg_i_1_n_0 : STD_LOGIC;
  signal led_reg_i_2_n_0 : STD_LOGIC;
  signal proximo_estado : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \proximo_estado[0]_C_n_0\ : STD_LOGIC;
  signal \proximo_estado[0]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \proximo_estado[0]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \proximo_estado[0]_LDC_n_0\ : STD_LOGIC;
  signal \proximo_estado[1]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \proximo_estado[1]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \proximo_estado[1]_LDC_n_0\ : STD_LOGIC;
  signal \proximo_estado[1]_P_n_0\ : STD_LOGIC;
  signal \proximo_estado[2]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \proximo_estado[2]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \proximo_estado[2]_LDC_n_0\ : STD_LOGIC;
  signal \proximo_estado[2]_P_n_0\ : STD_LOGIC;
  signal reset_IBUF : STD_LOGIC;
  signal reset_timer : STD_LOGIC;
  signal reset_timer_i_1_n_0 : STD_LOGIC;
  signal \timer[0]_i_1_n_0\ : STD_LOGIC;
  signal \timer[1]_i_1_n_0\ : STD_LOGIC;
  signal \timer_reg_n_0_[0]\ : STD_LOGIC;
  signal \timer_reg_n_0_[1]\ : STD_LOGIC;
  signal \NLW_cont_reg[25]_i_4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cont_reg[25]_i_4_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cont[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cont[10]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \cont[11]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \cont[12]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \cont[13]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \cont[14]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \cont[15]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \cont[16]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \cont[17]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \cont[18]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \cont[19]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \cont[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cont[20]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \cont[21]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \cont[22]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \cont[23]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \cont[24]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \cont[25]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \cont[2]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cont[3]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cont[4]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \cont[5]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \cont[6]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \cont[7]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \cont[8]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \cont[9]_i_1\ : label is "soft_lutpair6";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \cont_reg[12]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \cont_reg[16]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \cont_reg[20]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \cont_reg[24]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \cont_reg[25]_i_4\ : label is 35;
  attribute ADDER_THRESHOLD of \cont_reg[4]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \cont_reg[8]_i_2\ : label is 35;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of led_int_reg : label is "LD";
  attribute XILINX_LEGACY_PRIM of led_reg : label is "LD";
  attribute SOFT_HLUTNM of led_reg_i_2 : label is "soft_lutpair0";
  attribute XILINX_LEGACY_PRIM of \proximo_estado[0]_LDC\ : label is "LDC";
  attribute SOFT_HLUTNM of \proximo_estado[0]_LDC_i_1\ : label is "soft_lutpair0";
  attribute XILINX_LEGACY_PRIM of \proximo_estado[1]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \proximo_estado[2]_LDC\ : label is "LDC";
  attribute SOFT_HLUTNM of \timer[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \timer[1]_i_1\ : label is "soft_lutpair1";
begin
botao_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => botao_IBUF,
      O => botao_IBUF_BUFG
    );
botao_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => botao,
      O => botao_IBUF
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk,
      O => clk_IBUF
    );
\cont[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cont[25]_i_3_n_0\,
      I1 => \cont_reg_n_0_[0]\,
      O => cont(0)
    );
\cont[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[12]_i_2_n_6\,
      O => cont(10)
    );
\cont[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[12]_i_2_n_5\,
      O => cont(11)
    );
\cont[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[12]_i_2_n_4\,
      O => cont(12)
    );
\cont[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[16]_i_2_n_7\,
      O => cont(13)
    );
\cont[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[16]_i_2_n_6\,
      O => cont(14)
    );
\cont[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[16]_i_2_n_5\,
      O => cont(15)
    );
\cont[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[16]_i_2_n_4\,
      O => cont(16)
    );
\cont[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[20]_i_2_n_7\,
      O => cont(17)
    );
\cont[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[20]_i_2_n_6\,
      O => cont(18)
    );
\cont[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[20]_i_2_n_5\,
      O => cont(19)
    );
\cont[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[4]_i_2_n_7\,
      O => cont(1)
    );
\cont[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[20]_i_2_n_4\,
      O => cont(20)
    );
\cont[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[24]_i_2_n_7\,
      O => cont(21)
    );
\cont[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[24]_i_2_n_6\,
      O => cont(22)
    );
\cont[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[24]_i_2_n_5\,
      O => cont(23)
    );
\cont[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[24]_i_2_n_4\,
      O => cont(24)
    );
\cont[25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[25]_i_4_n_7\,
      O => cont(25)
    );
\cont[25]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFEF"
    )
        port map (
      I0 => \cont_reg_n_0_[1]\,
      I1 => \cont_reg_n_0_[24]\,
      I2 => \cont_reg_n_0_[25]\,
      I3 => \cont_reg_n_0_[3]\,
      I4 => \cont_reg_n_0_[2]\,
      O => \cont[25]_i_10_n_0\
    );
\cont[25]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => reset_IBUF,
      I1 => reset_timer,
      O => \cont[25]_i_2_n_0\
    );
\cont[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \cont[25]_i_5_n_0\,
      I1 => \cont[25]_i_6_n_0\,
      I2 => \cont[25]_i_7_n_0\,
      I3 => \cont[25]_i_8_n_0\,
      I4 => \cont[25]_i_9_n_0\,
      I5 => \cont[25]_i_10_n_0\,
      O => \cont[25]_i_3_n_0\
    );
\cont[25]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => \cont_reg_n_0_[17]\,
      I1 => \cont_reg_n_0_[16]\,
      I2 => \cont_reg_n_0_[19]\,
      I3 => \cont_reg_n_0_[18]\,
      O => \cont[25]_i_5_n_0\
    );
\cont[25]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \cont_reg_n_0_[21]\,
      I1 => \cont_reg_n_0_[20]\,
      I2 => \cont_reg_n_0_[23]\,
      I3 => \cont_reg_n_0_[22]\,
      O => \cont[25]_i_6_n_0\
    );
\cont[25]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cont_reg_n_0_[9]\,
      I1 => \cont_reg_n_0_[8]\,
      I2 => \cont_reg_n_0_[11]\,
      I3 => \cont_reg_n_0_[10]\,
      O => \cont[25]_i_7_n_0\
    );
\cont[25]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \cont_reg_n_0_[13]\,
      I1 => \cont_reg_n_0_[12]\,
      I2 => \cont_reg_n_0_[15]\,
      I3 => \cont_reg_n_0_[14]\,
      O => \cont[25]_i_8_n_0\
    );
\cont[25]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \cont_reg_n_0_[5]\,
      I1 => \cont_reg_n_0_[4]\,
      I2 => \cont_reg_n_0_[7]\,
      I3 => \cont_reg_n_0_[6]\,
      O => \cont[25]_i_9_n_0\
    );
\cont[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[4]_i_2_n_6\,
      O => cont(2)
    );
\cont[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[4]_i_2_n_5\,
      O => cont(3)
    );
\cont[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[4]_i_2_n_4\,
      O => cont(4)
    );
\cont[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[8]_i_2_n_7\,
      O => cont(5)
    );
\cont[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[8]_i_2_n_6\,
      O => cont(6)
    );
\cont[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[8]_i_2_n_5\,
      O => cont(7)
    );
\cont[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[8]_i_2_n_4\,
      O => cont(8)
    );
\cont[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => \cont_reg_n_0_[0]\,
      I1 => \cont[25]_i_3_n_0\,
      I2 => \cont_reg[12]_i_2_n_7\,
      O => cont(9)
    );
\cont_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(0),
      Q => \cont_reg_n_0_[0]\
    );
\cont_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(10),
      Q => \cont_reg_n_0_[10]\
    );
\cont_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(11),
      Q => \cont_reg_n_0_[11]\
    );
\cont_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(12),
      Q => \cont_reg_n_0_[12]\
    );
\cont_reg[12]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cont_reg[8]_i_2_n_0\,
      CO(3) => \cont_reg[12]_i_2_n_0\,
      CO(2) => \cont_reg[12]_i_2_n_1\,
      CO(1) => \cont_reg[12]_i_2_n_2\,
      CO(0) => \cont_reg[12]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cont_reg[12]_i_2_n_4\,
      O(2) => \cont_reg[12]_i_2_n_5\,
      O(1) => \cont_reg[12]_i_2_n_6\,
      O(0) => \cont_reg[12]_i_2_n_7\,
      S(3) => \cont_reg_n_0_[12]\,
      S(2) => \cont_reg_n_0_[11]\,
      S(1) => \cont_reg_n_0_[10]\,
      S(0) => \cont_reg_n_0_[9]\
    );
\cont_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(13),
      Q => \cont_reg_n_0_[13]\
    );
\cont_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(14),
      Q => \cont_reg_n_0_[14]\
    );
\cont_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(15),
      Q => \cont_reg_n_0_[15]\
    );
\cont_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(16),
      Q => \cont_reg_n_0_[16]\
    );
\cont_reg[16]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cont_reg[12]_i_2_n_0\,
      CO(3) => \cont_reg[16]_i_2_n_0\,
      CO(2) => \cont_reg[16]_i_2_n_1\,
      CO(1) => \cont_reg[16]_i_2_n_2\,
      CO(0) => \cont_reg[16]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cont_reg[16]_i_2_n_4\,
      O(2) => \cont_reg[16]_i_2_n_5\,
      O(1) => \cont_reg[16]_i_2_n_6\,
      O(0) => \cont_reg[16]_i_2_n_7\,
      S(3) => \cont_reg_n_0_[16]\,
      S(2) => \cont_reg_n_0_[15]\,
      S(1) => \cont_reg_n_0_[14]\,
      S(0) => \cont_reg_n_0_[13]\
    );
\cont_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(17),
      Q => \cont_reg_n_0_[17]\
    );
\cont_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(18),
      Q => \cont_reg_n_0_[18]\
    );
\cont_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(19),
      Q => \cont_reg_n_0_[19]\
    );
\cont_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(1),
      Q => \cont_reg_n_0_[1]\
    );
\cont_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(20),
      Q => \cont_reg_n_0_[20]\
    );
\cont_reg[20]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cont_reg[16]_i_2_n_0\,
      CO(3) => \cont_reg[20]_i_2_n_0\,
      CO(2) => \cont_reg[20]_i_2_n_1\,
      CO(1) => \cont_reg[20]_i_2_n_2\,
      CO(0) => \cont_reg[20]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cont_reg[20]_i_2_n_4\,
      O(2) => \cont_reg[20]_i_2_n_5\,
      O(1) => \cont_reg[20]_i_2_n_6\,
      O(0) => \cont_reg[20]_i_2_n_7\,
      S(3) => \cont_reg_n_0_[20]\,
      S(2) => \cont_reg_n_0_[19]\,
      S(1) => \cont_reg_n_0_[18]\,
      S(0) => \cont_reg_n_0_[17]\
    );
\cont_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(21),
      Q => \cont_reg_n_0_[21]\
    );
\cont_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(22),
      Q => \cont_reg_n_0_[22]\
    );
\cont_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(23),
      Q => \cont_reg_n_0_[23]\
    );
\cont_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(24),
      Q => \cont_reg_n_0_[24]\
    );
\cont_reg[24]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cont_reg[20]_i_2_n_0\,
      CO(3) => \cont_reg[24]_i_2_n_0\,
      CO(2) => \cont_reg[24]_i_2_n_1\,
      CO(1) => \cont_reg[24]_i_2_n_2\,
      CO(0) => \cont_reg[24]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cont_reg[24]_i_2_n_4\,
      O(2) => \cont_reg[24]_i_2_n_5\,
      O(1) => \cont_reg[24]_i_2_n_6\,
      O(0) => \cont_reg[24]_i_2_n_7\,
      S(3) => \cont_reg_n_0_[24]\,
      S(2) => \cont_reg_n_0_[23]\,
      S(1) => \cont_reg_n_0_[22]\,
      S(0) => \cont_reg_n_0_[21]\
    );
\cont_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(25),
      Q => \cont_reg_n_0_[25]\
    );
\cont_reg[25]_i_4\: unisim.vcomponents.CARRY4
     port map (
      CI => \cont_reg[24]_i_2_n_0\,
      CO(3 downto 0) => \NLW_cont_reg[25]_i_4_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_cont_reg[25]_i_4_O_UNCONNECTED\(3 downto 1),
      O(0) => \cont_reg[25]_i_4_n_7\,
      S(3 downto 1) => B"000",
      S(0) => \cont_reg_n_0_[25]\
    );
\cont_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(2),
      Q => \cont_reg_n_0_[2]\
    );
\cont_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(3),
      Q => \cont_reg_n_0_[3]\
    );
\cont_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(4),
      Q => \cont_reg_n_0_[4]\
    );
\cont_reg[4]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cont_reg[4]_i_2_n_0\,
      CO(2) => \cont_reg[4]_i_2_n_1\,
      CO(1) => \cont_reg[4]_i_2_n_2\,
      CO(0) => \cont_reg[4]_i_2_n_3\,
      CYINIT => \cont_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3) => \cont_reg[4]_i_2_n_4\,
      O(2) => \cont_reg[4]_i_2_n_5\,
      O(1) => \cont_reg[4]_i_2_n_6\,
      O(0) => \cont_reg[4]_i_2_n_7\,
      S(3) => \cont_reg_n_0_[4]\,
      S(2) => \cont_reg_n_0_[3]\,
      S(1) => \cont_reg_n_0_[2]\,
      S(0) => \cont_reg_n_0_[1]\
    );
\cont_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(5),
      Q => \cont_reg_n_0_[5]\
    );
\cont_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(6),
      Q => \cont_reg_n_0_[6]\
    );
\cont_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(7),
      Q => \cont_reg_n_0_[7]\
    );
\cont_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(8),
      Q => \cont_reg_n_0_[8]\
    );
\cont_reg[8]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cont_reg[4]_i_2_n_0\,
      CO(3) => \cont_reg[8]_i_2_n_0\,
      CO(2) => \cont_reg[8]_i_2_n_1\,
      CO(1) => \cont_reg[8]_i_2_n_2\,
      CO(0) => \cont_reg[8]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cont_reg[8]_i_2_n_4\,
      O(2) => \cont_reg[8]_i_2_n_5\,
      O(1) => \cont_reg[8]_i_2_n_6\,
      O(0) => \cont_reg[8]_i_2_n_7\,
      S(3) => \cont_reg_n_0_[8]\,
      S(2) => \cont_reg_n_0_[7]\,
      S(1) => \cont_reg_n_0_[6]\,
      S(0) => \cont_reg_n_0_[5]\
    );
\cont_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => cont(9),
      Q => \cont_reg_n_0_[9]\
    );
\display_7seg_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => display_7seg(0)
    );
\display_7seg_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => display_7seg(1)
    );
\display_7seg_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => display_7seg(2)
    );
\display_7seg_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => display_7seg(3)
    );
\display_7seg_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => display_7seg(4)
    );
\display_7seg_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => display_7seg(5)
    );
\display_7seg_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => display_7seg(6)
    );
\estado[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \proximo_estado[0]_LDC_n_0\,
      I1 => \proximo_estado[0]_C_n_0\,
      O => proximo_estado(0)
    );
\estado[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \proximo_estado[1]_LDC_n_0\,
      I1 => \proximo_estado[1]_P_n_0\,
      O => proximo_estado(1)
    );
\estado[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \proximo_estado[2]_LDC_n_0\,
      I1 => \proximo_estado[2]_P_n_0\,
      O => proximo_estado(2)
    );
\estado_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => proximo_estado(0),
      Q => estado(0)
    );
\estado_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => proximo_estado(1),
      Q => estado(1)
    );
\estado_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => proximo_estado(2),
      Q => estado(2)
    );
led_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => led_OBUF,
      O => led
    );
led_int_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => '0',
      G => led_reg_i_2_n_0,
      GE => '1',
      Q => led_OBUF
    );
led_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => led_reg_i_1_n_0,
      G => led_reg_i_2_n_0,
      GE => '1',
      Q => led_OBUF
    );
led_reg_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \timer_reg_n_0_[0]\,
      I1 => \timer_reg_n_0_[1]\,
      O => led_reg_i_1_n_0
    );
led_reg_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => estado(2),
      I1 => estado(0),
      I2 => estado(1),
      O => led_reg_i_2_n_0
    );
\proximo_estado[0]_C\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => botao_IBUF_BUFG,
      CE => '1',
      CLR => \proximo_estado[0]_LDC_i_2_n_0\,
      D => '1',
      Q => \proximo_estado[0]_C_n_0\
    );
\proximo_estado[0]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \proximo_estado[0]_LDC_i_2_n_0\,
      D => '1',
      G => \proximo_estado[0]_LDC_i_1_n_0\,
      GE => '1',
      Q => \proximo_estado[0]_LDC_n_0\
    );
\proximo_estado[0]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00002AAA"
    )
        port map (
      I0 => estado(1),
      I1 => \timer_reg_n_0_[1]\,
      I2 => \timer_reg_n_0_[0]\,
      I3 => estado(0),
      I4 => estado(2),
      O => \proximo_estado[0]_LDC_i_1_n_0\
    );
\proximo_estado[0]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000FF0"
    )
        port map (
      I0 => \timer_reg_n_0_[1]\,
      I1 => \timer_reg_n_0_[0]\,
      I2 => estado(2),
      I3 => estado(0),
      I4 => estado(1),
      O => \proximo_estado[0]_LDC_i_2_n_0\
    );
\proximo_estado[1]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \proximo_estado[1]_LDC_i_2_n_0\,
      D => '1',
      G => \proximo_estado[1]_LDC_i_1_n_0\,
      GE => '1',
      Q => \proximo_estado[1]_LDC_n_0\
    );
\proximo_estado[1]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00007FAA"
    )
        port map (
      I0 => estado(0),
      I1 => \timer_reg_n_0_[0]\,
      I2 => \timer_reg_n_0_[1]\,
      I3 => estado(1),
      I4 => estado(2),
      O => \proximo_estado[1]_LDC_i_1_n_0\
    );
\proximo_estado[1]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00558000"
    )
        port map (
      I0 => estado(1),
      I1 => \timer_reg_n_0_[1]\,
      I2 => \timer_reg_n_0_[0]\,
      I3 => estado(0),
      I4 => estado(2),
      O => \proximo_estado[1]_LDC_i_2_n_0\
    );
\proximo_estado[1]_P\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => botao_IBUF_BUFG,
      CE => '1',
      D => '0',
      PRE => \proximo_estado[1]_LDC_i_1_n_0\,
      Q => \proximo_estado[1]_P_n_0\
    );
\proximo_estado[2]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \proximo_estado[2]_LDC_i_2_n_0\,
      D => '1',
      G => \proximo_estado[2]_LDC_i_1_n_0\,
      GE => '1',
      Q => \proximo_estado[2]_LDC_n_0\
    );
\proximo_estado[2]_LDC_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00458000"
    )
        port map (
      I0 => estado(1),
      I1 => \timer_reg_n_0_[1]\,
      I2 => \timer_reg_n_0_[0]\,
      I3 => estado(0),
      I4 => estado(2),
      O => \proximo_estado[2]_LDC_i_1_n_0\
    );
\proximo_estado[2]_LDC_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00047FAA"
    )
        port map (
      I0 => estado(0),
      I1 => \timer_reg_n_0_[0]\,
      I2 => \timer_reg_n_0_[1]\,
      I3 => estado(1),
      I4 => estado(2),
      O => \proximo_estado[2]_LDC_i_2_n_0\
    );
\proximo_estado[2]_P\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => botao_IBUF_BUFG,
      CE => '1',
      D => '0',
      PRE => \proximo_estado[2]_LDC_i_1_n_0\,
      Q => \proximo_estado[2]_P_n_0\
    );
reset_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => reset,
      O => reset_IBUF
    );
reset_timer_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => estado(2),
      I1 => estado(0),
      I2 => estado(1),
      I3 => reset_timer,
      O => reset_timer_i_1_n_0
    );
reset_timer_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => botao_IBUF_BUFG,
      CE => '1',
      D => reset_timer_i_1_n_0,
      Q => reset_timer,
      R => '0'
    );
\timer[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => \cont[25]_i_3_n_0\,
      I1 => \cont_reg_n_0_[0]\,
      I2 => \timer_reg_n_0_[0]\,
      O => \timer[0]_i_1_n_0\
    );
\timer[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FD02"
    )
        port map (
      I0 => \timer_reg_n_0_[0]\,
      I1 => \cont_reg_n_0_[0]\,
      I2 => \cont[25]_i_3_n_0\,
      I3 => \timer_reg_n_0_[1]\,
      O => \timer[1]_i_1_n_0\
    );
\timer_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => \timer[0]_i_1_n_0\,
      Q => \timer_reg_n_0_[0]\
    );
\timer_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => \cont[25]_i_2_n_0\,
      D => \timer[1]_i_1_n_0\,
      Q => \timer_reg_n_0_[1]\
    );
end STRUCTURE;
