----------------------------------------------------------------------------------
-- Company: UFRGS
-- Engineer: Jonathan Silva Martins
-- 
-- Create Date: 10/18/2020 09:12:56 PM
-- Design Name: 
-- Module Name: tb_maquina_de_estados - Behavioral
-- Project Name: Trabalho 2 - Sistemas Digitais - 2020/1
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_maquina_de_estados is
--  Port ( );
end tb_maquina_de_estados;

architecture Behavioral of tb_maquina_de_estados is

component maquina_de_estados is
  Port ( botao : in std_logic;
         reset : in std_logic;
         clk : in std_logic;
         led : out std_logic;
         display_7seg : out std_logic_vector(6 downto 0) );
end component;

signal botao, reset, clk, led : std_logic;
signal display_7seg : std_logic_vector(6 downto 0);

begin

maquina_de_estados1: maquina_de_estados
    Port map ( botao => botao,
               reset => reset,
               clk => clk,
               led => led,
               display_7seg => display_7seg );
               
process
begin
    clk <= '0';
    wait for 10ns;
    clk <= '1';
    wait for 10ns;
end process;

process
begin
    botao <= '0';
    reset<='1';
    wait for 20ns;
    reset<='0';
    wait for 20ns;
    for i in 0 to 3000 loop
        wait for 50ms;
        botao <= '1';
        wait for 50ms;
        botao <= '0';
    end loop;
end process;
end Behavioral;
