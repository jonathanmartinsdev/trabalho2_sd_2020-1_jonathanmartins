----------------------------------------------------------------------------------
-- Company: UFRGS
-- Engineer: Jonathan Silva Martins
-- 
-- Create Date: 10/18/2020 09:12:56 PM
-- Design Name: 
-- Module Name: maquina_de_estados - Behavioral
-- Project Name: Trabalho 2 - Sistemas Digitais - 2020/1
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity maquina_de_estados is
  Port ( botao : in std_logic;
         reset : in std_logic;
         clk : in std_logic;
         led : out std_logic;
         display_7seg : out std_logic_vector(6 downto 0) );
end maquina_de_estados;

architecture Behavioral of maquina_de_estados is
-- Definicao dos estados usados
type tipo_estado is (
    estado_inicial, 
    estado_botao_pressionado_1_vez,
    estado_botao_pressionado_2_vezes, 
    estado_led_aceso,
    estado_contagem_display_7seg );
    
-- Variaveis usadas para o controle da maquina de estado
signal estado, proximo_estado : tipo_estado;
signal reset_timer, enable_timer, led_int : std_logic;
signal timer : std_logic_vector(1 downto 0);
signal saida : std_logic_vector(3 downto 0);

-- A variavel cont vai servir para contar ate 60.000.000 que vai ser 1 segundo a 50MHz, dessa forma servindo como um temporizador
signal cont : std_logic_vector(25 downto 0);

-- O componente conversor foi feito no trabalho 1 e vai me permitir tratar a saida como o numero que eh mostrado no display.
component conversor_nibble_7seg is
    Port ( entrada : in STD_LOGIC_VECTOR (3 downto 0);
           saida : out STD_LOGIC_VECTOR (6 downto 0));
end component;
begin

conversor: conversor_nibble_7seg
    Port map ( 
        entrada => saida,
        saida => display_7seg
     );
     
led <= led_int;

process (clk, reset)
begin
    if reset='1' then
        estado <= estado_inicial;
    elsif rising_edge(clk) then
        estado <= proximo_estado;
    end if;
end process;

process (botao, estado)
begin
    reset_timer <= '0';
    led_int <= '0';
    saida <= saida;
    enable_timer <= '0';
    case estado is
        when estado_inicial =>
            saida <= "0000";
            proximo_estado <= estado_inicial;
            if falling_edge(botao) then
                proximo_estado <= estado_botao_pressionado_1_vez;
            end if;
        when estado_botao_pressionado_1_vez =>
            proximo_estado <= estado_botao_pressionado_1_vez;
            if falling_edge(botao) then
                proximo_estado <= estado_botao_pressionado_2_vezes;
            end if;
        when estado_botao_pressionado_2_vezes =>
            proximo_estado <= estado_botao_pressionado_2_vezes;
            if falling_edge(botao) then
                reset_timer <= '1';
                enable_timer <= '1';
                proximo_estado <= estado_led_aceso;
            end if;
        when estado_led_aceso =>
            enable_timer <= '1';
            proximo_estado <= estado_led_aceso;
            led_int <= '1';
            if timer="11" then
                reset_timer <= '1';
                proximo_estado <= estado_contagem_display_7seg;
            end if;
        when estado_contagem_display_7seg =>
            enable_timer <= '1';
            proximo_estado <= estado_contagem_display_7seg;
            if timer="01" then
                if saida = "1001" then
                    saida <= "0000";
                    reset_timer <= '1';
                    proximo_estado <= estado_inicial;
                else
                    saida <= std_logic_vector(unsigned(saida) + 1);
                    reset_timer <= '1';
                end if;
            end if;
    end case;
end process;

process(clk, reset_timer, reset, enable_timer)
begin
    cont <= cont;
    timer <= timer;
    if reset='1' then
        cont <= "00000000000000000000000000";
        timer <= "00";
    elsif reset_timer='1' then
        cont <= "00000000000000000000000000";
        timer <= "00";
    elsif rising_edge(clk) then
        if enable_timer='1' then
            cont <= std_logic_vector(unsigned(cont) + 1);
            -- Quando cont for igual a 50.000.000, isso significa que se passou 1 segundo
            if  cont = "10111110101111000010000000" then
                timer <= std_logic_vector(unsigned(timer) + 1);
                cont <= "00000000000000000000000000";
            end if;
        end if;
    end if;
end process;
end Behavioral;
