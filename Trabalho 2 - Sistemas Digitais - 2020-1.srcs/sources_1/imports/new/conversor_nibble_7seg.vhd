----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/17/2020 10:11:38 AM
-- Design Name: 
-- Module Name: conversor_nibble_7seg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity conversor_nibble_7seg is
    Port ( entrada : in STD_LOGIC_VECTOR (3 downto 0);
           saida : out STD_LOGIC_VECTOR (6 downto 0));
end conversor_nibble_7seg;

architecture Behavioral of conversor_nibble_7seg is

begin
saida <= "1111110" when entrada="0000" else
         "0110000" when entrada="0001" else
         "1101101" when entrada="0010" else
         "1111001" when entrada="0011" else
         "0110011" when entrada="0100" else
         "1011011" when entrada="0101" else
         "1011111" when entrada="0110" else
         "1110000" when entrada="0111" else
         "1111111" when entrada="1000" else
         "1111011" when entrada="1001" else
         "1110111" when entrada="1010" else
         "0011111" when entrada="1011" else
         "0001101" when entrada="1100" else
         "0111101" when entrada="1101" else
         "1001111" when entrada="1110" else
         "1000111" when entrada="1111";
      
        
end Behavioral;
